/******************************************************************************
*                         AVENTURA 1 - SISTEMAS OPERATIVOS
*   FECHA:      22/10/18
*   AUTORES:    JUAN CARLOS BOO CRUGEIRAS.
*               HÉCTOR MARIO MEDINA CABANELAS.
*   ESTUDIOS:   GRADO EN INGENIERÍA INFORMÁTICA - UIB.
******************************************************************************/

/******************************************************************************                    
*                        INCLUÍMOS LAS LIBRERÍAS UTILIZADAS                        
******************************************************************************/

#include<stdio.h>   // Standard input/output library.
#include<string.h>  // Header library. Se usa para size_t.
#include<stdlib.h>  // Librería Standard, se usa para malloc() entre otras.
#include"my_lib.h"  // Librería personalizada.
 
/******************************************************************************
 *  TEST 1: 
*       size_t my_strlen(const char *str);
*       int my_strcmp(const char *str1, const char *str2);
*       char *my_strcpy(char *dest, const char *src);
*       char *my_strncpy(char *dest, const char *src, size_t n);
 *      char *my_strcat(char *dest, const char *src);
 * ***************************************************************************/ 


/******************************************************************************
 * Nombre:       my_strlen();
 * Descripción:  Calcula el número de caracteres de una cadena de caracteres 
 *               pasada por parámetro.
 * Parámetros:   const char *str: Puntero a la cadena de caracteres a evaluar.
 * Devuelve:     Un sizq_t que representa el número de caracteres de str.
 * ***************************************************************************/

size_t my_strlen(const char *str){
    // Declaramos variable auxiliar que nos sirve de contador.
    size_t contador = 0;
    // Mientras no encontremos el caracter final de cadena '\0'.
    while(*str){    // Equivalente a *str == '\0' 
        // Añadimos 1 al contador.
        contador++;
        // Nos posicionamos en el siguiente elemento de la cadena.
        str++;
    }
    // Devolvemos el contador.
    return contador;
}


/******************************************************************************
 * Nombre:       my_strcmp();
 * Descripción:  Compara dos cadenas de caracteres y devuelve un valor
 *               dependiendo de la operación de comparación.
 * Parámetros:   const char *str1: Puntero a cadena de caracteres para comparar
 *               const char *str2: Puntero a cadena de caracteres para comparar
 * Devuelve:     valor < 0 si cadena1 < cadena2.
 *               valor = 0 si cadena1 = cadena2.
 *               valor > 0 si cadena1 > cadena2.
 * ***************************************************************************/

int my_strcmp(const char *str1, const char *str2){
    // Al principio suponemos que son iguales.
    int result = 0;
    // Mientras no llegamos al fin de las cadenas y no tengan un caracter 
    // distinto como resultado de la comparación de caracteres anteriores.
    while(*str1 && *str2 && result == 0){
        // Se resta los valores ASCI de ambos caracteres.
        result = *str1 - *str2;
        // Nos posicionamos en el siguiente caracter.
        str1++;
        str2++;
    }
    // Devolvemos el resultado de la comparación.
    return result;
}



/******************************************************************************
 * Nombre:       my_strcpy();
 * Descripción:  Copia el vector src en el vector dest ambos pasados por 
 *               parámetro. 
 * Parámetros:   char *dest: Puntero a cadena de caracteres destino.
 *               const char *src: Puntero a cadena de caracteres fuente.
 * Devuelve:     Puntero char a cadena de caracteres destino.
 * ***************************************************************************/

char *my_strcpy(char *dest, const char *src){
    // Creamos un vector que apunte al primer caracter de dest.
    char *dest2 = dest;
    // Mientras no lleguemos al caracter final de cadena '\0'
    while(*src){    // Equivalente a *src = '\0' 
        // Copiamos el contenido de src como contenido de dest.
        *dest = *src;
        // Apuntamos al siguiente elemento de ambos vectores.
        dest++;
        src++;
    }
    // Añadimos el caracter de final de línea '\0' a dest.
    *dest = '\0';
    // Devolvemos el vector dest desde el principio a través de dest2.
    return dest2;
}


/******************************************************************************
 * Nombre:       my_strncpy();
 * Descripción:  Copia el número indicado de caracteres del vector *src en el 
 *               vector *dest ambos pasados por parámetro.
 * Parámetros:   char *dest: Puntero a cadena de caracteres destino.
 *               const char *src: Puntero a cadena de caracteres fuente.
 *               size_t n: Número de caracteres que deseamos copiar.
 * Devuelve:     Puntero char a cadena de caracteres destino
 * ***************************************************************************/

char *my_strncpy(char *dest, const char *src, size_t n){
    char *dest2 = dest;
    // Bucle que recorre *src un número n de veces.
    for(int i = 0; (*src) && (i < n); i++){  
        // Copiamos el contenido de src como contenido de dest.
        *dest = *src;
        // Apuntamos al siguiente elemento de ambos vectores.
        dest++;
        src++;
    }
    // Si hemos llegado al final de src, es decir src.lenght < n
    if(*src == '\0'){
        // Ponemos el caracter final de línea en dest.
        *dest = '\0';
    }
    return dest2;    
}


/******************************************************************************
 * Nombre:       my_strcat();
 * Descripción:  Concatena la cadena apuntada por src a la cadena apuntada por
 *               dest, ambas enviadas como parámetro.
 * Parámetros:   char *dest: Puntero a cadena de caracteres destino.
 *               const char *src: Puntero a cadena de caracteres fuente.
 * Devuelve:     Puntero char a cadena de caracteres destino
 * ***************************************************************************/

char *my_strcat(char *dest, const char *src){
    // Guardamos el puntero dest para devolverlo al final
    char *dest2 = dest;
    // Hacemos que el puntero avance hasta el final de la cadena
    while(*dest){
        dest++;
    }
    // Llegamos al final de la cadena, empezamos a llenarla con la otra
    while(*src){
        *dest = *src;
        dest++;
        src++;
    }
    dest = '\0';
    return dest2;
}


 
/******************************************************************************
 *  TEST 2: 
*       struct my_stack *my_stack_init(int size){
*       int my_stack_push(struct my_stack *stack, void *data){
*       void *my_stack_pop(struct my_stack *stack){
*       int my_stack_len(struct my_stack *stack){
*       int my_stack_write(struct my_stack *stack, char *filename){
*       struct my_stack *my_stack_read(char *filename){
*       int my_stack_purge (struct my_stack *stack){
 *****************************************************************************/ 


/******************************************************************************
 * Nombre:       my_stack_init();
 * Descripción:  Reserva espacio para la pila e inicializa los valores de la 
 *               pila con el tamaño de datos que nos pasan como parámetro y 
 *               con NULL como valor del puntero al primer nodo.
 * Parámetros:   int size: Determina el tamaño de la pila a inicializar.
 * Devuelve:     Devuelve un puntero a la pila inicializada.
 * ***************************************************************************/

struct my_stack *my_stack_init(int size){
    // Declaramos e inicializamos una pila nula y un nodo nulo.
    struct my_stack *pila = NULL;
    struct my_stack_node *nodo = NULL;
    // Asignamos el puntero que devuelve malloc a la pila, y reservamos 
    //  espacio según el parámetro de entrada size.
    pila = malloc(sizeof(size) + sizeof(nodo));    
    // Inicializamos propiedades de la pila conforme el parámetro size.
    pila->size = size;
    // El elemento de más abajo de la pila es un nodo nulo.
    pila->first = nodo;
    // Devolvemos la pila. 
    return pila;
}

/******************************************************************************
 * Nombre:       my_stack_push();
 * Descripción:  Inserta un nuevo nodo en la pila. El puntero a los datos 
 *               de ese nodo nos lo pasan como parámetro.
 *               La pila tiene que existir y el tamaño de los datos ha 
 *               de ser > 0.
 * Parámetros:   struct my_stack *stack: Puntero a la pila a utilizar.
 *               void *data: Puntero a los datos a utilizar.
 * Devuelve:     
 *               Devuelve 0 si ha ido bien, y -1 si hubo un error.El puntero
 *               a los datos del elemento eliminado, si no existe nodo
 *               superior (pila vacía) devuelve NULL.
 * ***************************************************************************/

int my_stack_push(struct my_stack *stack, void *data){
    // Declaramos e inicializamos un nuevo nodo null.
    struct my_stack_node *nodo = NULL;
    // Reservamos espacio en memoria para los datos y los introducimos 
    // en el nodo.
    // El nodo (puntero) apuntará a ese espacio reservado
    nodo = malloc(sizeof(data));    
    nodo->data = data;
    // La pila tiene que existir, si no devuelve -1.
    if (stack == NULL){
        return -1;
    }
    // Introducimos los valores del nodo.
    nodo->data = data;
    // Actualizamos los punteros de la pila para que apunten al nodo correcto.
    // stack->first ha de apuntar al elemento superior, es decir el que 
    // acabamos de generar. Y nodo->next deberá apuntar al antiguo elemento 
    // superior.
    nodo->next = stack->first;
    stack->first = nodo;
    // Devolvemos 0, porque no se han generado errores.
    return 0;
}

/******************************************************************************
 * Nombre:       my_stack_pop();
 * Descripción:  Elimina el nodo superior de la pila, liberando la memoria que
 *               ocupada ese nodo.
 * Parámetros:   struct my_stack *stack: Pila a utilizar.
 * Devuelve:     El puntero a los datos del elemento eliminado, si no existe
 *               nodo superior (pila vacía) devuelve NULL.
 * ***************************************************************************/

void *my_stack_pop(struct my_stack *stack){
    // Si no hay ningún nodo en la pila, devuelve NULL.
    if (stack->first == NULL){
        return NULL;
    }
    // Apuntamos al nodo con un puntero.
    struct my_stack_node *nodo = stack->first;
    // Guardamos en un puntero los datos del elemento eliminado.
    void *datos = nodo->data;
    // Sustituimos el nodo.
    free(stack->first);
    stack->first = nodo->next;
    // Devolvemos los datos que poseía el nodo.
    return datos;
}


/******************************************************************************
 * Nombre:       my_stack_len();
 * Descripción:  Recorre la pila y retorna el número de nodos totales que hay
 *               en la pila
 * Parámetros:   struct my_stack *stack: Pila a utilizar.
 * Devuelve:     El número de nodos de la pila.
 * ***************************************************************************/

int my_stack_len(struct my_stack *stack){
    // Declaramos e inicializamos la variable que cueta la cantidad de nodos.
    int nodos = 0;
    // Si la pila está vacía devolvemos 0.
    if(stack->first == NULL){
        return 0;
    }
    // Apuntamos al primer nodo.
    struct my_stack_node *nodo = stack->first; 
    // Aumentamos el número de nodos.
    nodos++;
    // Mientras siga habiendo nodos, sumamos y apuntamos al siguiente.
    while(nodo->next != NULL){
        nodos++;
        nodo = nodo->next;
    }
    // Devolvemos la cantidad de nodos.
    return nodos;
}


/******************************************************************************
 * Nombre:       my_stack_write();
 * Descripción:  Almacena los datos de la pila en el fichero indicado por 
 *               filename.
 * Parámetros:   struct my_stack *stack: Puntero a la pila a utilizar.
 *               char *filename: Puntero del nombre de fichero.
 * Devuelve:     El número de elementos almacenados o -1 si ha habido algún 
 *               error.
 * ***************************************************************************/

int my_stack_write( struct my_stack *stack, char *filename){
    // Generamos una pila auxiliar 'pila'.
    void *pila = my_stack_init(stack->size);
    // Llenamos la pila auxiliar en orden inverso.
    // Usaremos este puntero para acceder a la pila auxiliar.
    struct my_stack_node *index = stack->first;
    // Variable que almacena el código de error.
    int error; 
    // Mientras tengamos nodos no nulos (pila tenga elementos)
    while(index != NULL){
        // Guardamos elementos en la pila.
        error = my_stack_push(pila, index->data);
        if(error != 0){
            return -1;
        }
        // Actualizamos el índice.
        index = index->next;
    }
    // Abrimos el fichero.
    // Devuelve un int (fichero) que se usará para indentificar este fichero al
    // realizar operaciones
    int fichero = open(filename, 
                       O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR); 
    // Escribimos todo partiendo de la pila auxiliar.
    // Con esta variable comprobaremos si la cantidad de datos
    ssize_t size;   
    // escritos es la que corresponde, en caso de no serlo devolvemos error
    void *dato = my_stack_pop(pila); // Apuntamos a los datos del primer nodo
    int contador = 0;
    size = write(fichero, &stack->size, sizeof(stack->size));
    if(size != sizeof(stack->size)){
            return -1;
    }
    while(dato != NULL){
        size = write(fichero, dato, stack->size);
        contador++;
        // Comprobamos si la escritura ha ido bien
        if(size != stack->size){
            return -1;
        }
        dato = my_stack_pop(pila);
    }
    // CERRAMOS EL FICHERO Y COMPROBAMOS SI HA HABIDO ALGÚN ERROR
    // Si la funcion "close()" devuelve un -1 es que ha habido algún error
    int cerrar = close (fichero);
    if(cerrar == -1){   
        return -1;
    }
    // SI TODO HA SALIDO BIEN, DEVOLVEMOS 0
    return contador;
}

/******************************************************************************
 * Nombre:       my_stack_read();
 * Descripción:  Lee los datos de la pila almacenados en el fichero, 
 *               inicializa la pila, la construye en memoria. 
 * Parámetros:   char *filename: Puntero del nombre de fichero.
 * Devuelve:     Puntero a la pila o NULL si ha habido algún error.
 * ***************************************************************************/

struct my_stack *my_stack_read(char *filename){
    // Abrimos el fichero.
    // Devuelve un int (fichero) que se usará para indentificar este fichero 
    // al realizar operaciones
    int fichero = open(filename, O_RDONLY); 
    // Si hubo algun problema en la apertura, devolvemos NULL.
    if (fichero < 0) {
        return NULL;
    }
    // Obtenemos el tamaño de la pila del fichero para inicializarla.
    int *bytes = malloc(sizeof(int));
    int size = read(fichero, bytes, sizeof(int));
    struct my_stack *pila = my_stack_init(*bytes);
    // Accedemos ahora al fichero y guardamos los datos en la pila.
    // Comprobamos errores / fin de fichero con la variable 'size'.
    int error = 0; 
    void *index;
    while(size != 0){
        index = malloc(pila->size);
        size = read(fichero, index, pila->size);
        if((size != pila->size) & (size != 0)){
            return NULL;
        }
        // si el fichero no se ha acabado generamos un nuevo nodo con los datos
        if(size != 0){  
            //comprobamos errores con la variable 'error'
            error = my_stack_push(pila, index);
            if(error != 0){
            return NULL;
            }
        }
    }
    // Cerramos el fichero y comprbamos si ha habido algún error.
    int cerrar = close(fichero);
    // SI la funcion "close()" devuelve un -1 es que ha habido algún error
    if(cerrar == -1){   
        return NULL;
    }
    // Si todo ha ido bien, devolvemos el puntero de la pila.
    return pila;
}

/******************************************************************************
 * Nombre:       my_stack_purge();
 * Descripción:  Libera la memoria de los datos, la de la estructura del 
 *               nodo y la de la pila.
 * Parámetros:   struct my_stack *stack: Puntero de la pila a utilizar.
 * Devuelve:     Devuelve el número de nodos liberados.
 * ***************************************************************************/
int my_stack_purge (struct my_stack *stack){
    // Guardaremos el número de nodos liberados.
    int contador = 0;  
    // Variable para evaluar cada nodo.
    struct my_stack_node *node = stack->first;
    // Mientras el nodo no sea nulo no habremos llegado al final de la pila.
    while(node != NULL){
        // Liberamos memoria del nodo.
        free(node);
        // Pasamos al siguiente nodo de la pila.
        node = node->next;
        // Aumentamos el contador de nodos liberados.
        contador++;
    }
    free(stack);
    // Devolvemos el número de nodos liberados.
    return contador;
}
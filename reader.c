/******************************************************************************
*                                   READER
*                       SISTEMAS OPERATIVOS: AVENTURA 3.
*                    GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       13 DE DICIEMBRE DE 2018.
*
 *****************************************************************************/
/******************************************************************************
 *                          LIBRERÍAS NECESARIAS 
 *****************************************************************************/
#include <stdio.h>
#include <limits.h>
#include "my_lib.h"
/******************************************************************************
 *                                  MAIN 
 *****************************************************************************/
int main(int argc, char * argv[]){
    // Comprobamos qu ela sintaxis es correcta
    if(argc != 2){
        printf("USAGE: %s filename\n", argv[0]);
        return -1;
    } 
    else{
        struct my_stack *pila = NULL;
        // Guardamos la pila del fichero que se indica por parámetro
        pila = my_stack_read(argv[1]);
        // Si hubo problemas al leer el fichero, error
        if(pila == NULL){
            printf("Couldn't open stack file s\n");
            exit(0);
        }
        // Guardamos la distancia de la pila
        int length = my_stack_len(pila);
        printf("stack length: %d\n", length);
        // Inicializamos valores que necesitaremos
        int *actual;
        int sum = 0;
        int min = INT_MAX;
        int max = 0;
        // Accedemos a la pila y recogemos los valores
        for(int a = 0; a < length; a++){
            actual = my_stack_pop(pila);
            printf("%d\n", *actual);
            sum += *actual;
            if(*actual > max) max = *actual;
            if(*actual < min) min = *actual;
        }
        int average = sum / length;
        printf("Items: %d Sum: %d Min: %d Max: %d Average: %d\n", length, sum, min, max, average);
    }
    return 1;
}
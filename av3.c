/******************************************************************************
*                                 MULTIHILO
*                       SISTEMAS OPERATIVOS: AVENTURA 3.
*                    GRADO EN INGENIERÍA INFORMÁTICA - UIB
*
*   AUTORES:     - JUAN CARLOS BOO CRUGEIRAS (carlosboocrugeiras@gmail.com)
*                - HÉCTOR MARIO MEDINA CABANELAS (hmedcab@gmail.com)
*   
*   FECHA:       10 DE DICIEMBRE DE 2018.
*
 *****************************************************************************/
/******************************************************************************
 *                          LIBRERÍAS NECESARIAS 
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "my_lib.h"
/******************************************************************************
 *                       DEFINICIONES DE VARIABLES  
 *****************************************************************************/
#define STACK_SIZE  10
const int NUM_HILOS  = 10;
const int ITERATIONS = 10000000;
struct my_stack *pila = NULL;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
/******************************************************************************
 *                          FUNCIONES UTILIZADAS  
 *****************************************************************************/
void threadTask();
/******************************************************************************
 *                                   MAIN
 *****************************************************************************/
int main(int argc, char * argv[]){
    pthread_t  hilo[NUM_HILOS];

    // Si no tiene dos elementos no se ha usado la sintaxis correcta.
    if(argc != 2){
        printf("USAGE: %s filename\n", argv[0]);
        return -1;
    }
    else{
        // Guardamos la pila del fichero que se indica por parámetro
        pila= my_stack_read(argv[1]);
        // Si no existe la pila, la creamos
        if(pila == NULL){
            // Creamos la pila
            pila = my_stack_init(STACK_SIZE);
            // Llenamos la pila con 10 nodos
            for(int i = 0; i < STACK_SIZE ; i++){
                int *data = malloc(sizeof(int));
                // Los nodos estarán inicializados con punteros int apuntando a 0
                *data = 0;
                my_stack_push(pila, data);
            }
            // La escribimos en el fichero indicado
            my_stack_write(pila, argv[1]);
        }
        // Guardamos el número de elementos de la pila
        int elementos = my_stack_len(pila);
        // Si la pila tiene menos de 10 elementos, la rellenamos
        if(elementos < STACK_SIZE){
            for(int a = elementos; a < STACK_SIZE; a++){
                printf("Elemento número: %d\n", a);
                int *data = malloc(sizeof(int));
                // Los nodos estarán inicializadrm os con punteros int apuntando a 0
                *data = 0;
                my_stack_push(pila, data);
            }
        }
        printf("Threads: %d, Iterations: %d\n", STACK_SIZE, ITERATIONS);
        // Generamos los hilos simultáneos
        for(int i = 0;i<NUM_HILOS;i++){
            pthread_create(&hilo[i], NULL, (void *) &threadTask, NULL);
        }
        // Hacemos que el programa principal espera a que los hilos acaben para continuar
        for(int i = 0; i < NUM_HILOS; i++){
           pthread_join(hilo[i], NULL);
        }
        // Guardamos la pila en el fichero
        my_stack_write(pila, argv[1]);
        // Final
        printf("Bye from main\n");
        pthread_exit(0);
        return 1;
    }
}
/******************************************************************************
*   NOMBRE:        THREADTASK();
*   ARGUMENTOS:    -
*   DEVUELVE:      VOID
*   DESCRIPCIÓN:   FUNCIÓN LA CUÁL REALIZARÁN TODOS LOS HILOS, CONSISTE EN
*                  ACCEDER A LA PILA, TOMAR UN NODO, AÑADIRLE UNO AL VALOR
*                  INT QUE CONTIENE DICHO NODO Y DEVOLVER EL NODO A LA PILA.
*                  COMO ESTA FUNCIÓN LA REALIZARÁN TODOS LOS HILOS 
*                  SIMULTÁNEAMENTE, LOS NODOS DEBEN EXTRAERSE E INTRODUCIRSE
*                  EN LA PILA UTILIZANDO UN SEMÁFORO.
 *****************************************************************************/
void threadTask(){
    printf("Starting thread\n");
    for(int i = 0; i < ITERATIONS ;i++){
        // Tomamos el último nodo de la pila (con semáforo)
        pthread_mutex_lock(&mutex);
        int *dato = my_stack_pop(pila);
        pthread_mutex_unlock(&mutex);
        // Sumamos uno al valor
        *dato = *dato +1;
        // Guardamos el nodo en la pila (con semáforo)
        pthread_mutex_lock(&mutex);
        my_stack_push(pila, dato);
        pthread_mutex_unlock(&mutex);
    }
}